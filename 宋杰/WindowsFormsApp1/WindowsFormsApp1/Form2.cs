﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }


        private int id;

        public Form2(int id, string name, int age, int score) 
        {
            InitializeComponent();

            this.id = id;
            textBox1.Text = name;
            textBox2.Text = age.ToString();
            textBox3.Text = score.ToString();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var age = textBox2.Text;
            var score = textBox3.Text;
            if (this.id > 0)
            {

                var sql = string.Format("update students set studentsname = '{0}',Age ={1} , Score ={2} where Id ={3}",name,age,score,this.id);

                DBhelp.AddOrUpdate(sql);

                MessageBox.Show("更新成功","提示");
            }
            else
            {
                var sql = string.Format("insert into students (studentsname, Age, Score) values('{0}',{1},{2})", name, age, score);

               DBhelp.AddOrUpdate(sql);

                MessageBox.Show("添加成功", "提示");
            }
            this.DialogResult = DialogResult.Yes;

            this.Close();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }
    }
}
