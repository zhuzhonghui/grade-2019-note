<?php
$dsn = "sqlsrv:Server=localhost;Database=Task";
$db = new PDO($dsn, "sa", "123456");
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>列表</title>
		<link rel="stylesheet" type="text/css" href="css/main.css" />
	</head>
	<body>
		<div id="container">
			<div id="login_info">
    欢迎你：admin
				<a href="logout.html">退出登录</a>
			</div>
            <div id="choose_div">
                <button id="all">全选</button>
                <a id="multi_delete" href="javascript:void(0);">删除选中任务</a>

                <a id="add" href="updata.php" style="float: right">增加任务</a>
            </div>
            <table class="list">
                <tbody>
                <tr>
                    <th></th>
                    <th>任务id</th>
                    <th>任务名称</th>
                    <th>任务状态</th>
                    <th>创建时间</th>
                    <th>修改时间</th>
                    <th>操作</th>
                </tr>
                <?php foreach ($taskList as $item): ?>
                <tr>
                    <td><input type="checkbox" class="task-checkbox" name="TaskId[]" value="<?php echo $item['TaskId'];?>"></td>
                    <td><?php echo $item['TaskId']?></td>
                    <td><?php echo $item['TaskName'] ?></td>
                    <td><?php echo $statusList[$item['TaskStatus ']]?></td>
                    <td><?php echo $item['TaskCreateTime']?></td>
                    <td><?php echo $item['TaskUpdateTime']?></td>
                    <td>

                    </td>
                </tr>
                <?php endforeach; ?>
                <tr>
                    <td><input type="checkbox" class="task-checkbox" name="TaskId[]" value=""></td>
                    <td>1</td>
                    <td>这个是一个任务</td>
                    <td>新创建</td>
                    <td>2021-01-03 20:21:00</td>
                    <td>2021-01-03 20:21:00</td>
                    <td>
                        <a class="update_ing" href="detail.php">详情</a>
                    </td>
                </tr>
                </tbody>
            </table>
		</div>
	</body>
</html>

