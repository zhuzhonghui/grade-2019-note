<?php
$taskId = $_GET['id'] ?? null;
if (empty($taskId)) {
    echo "任务id错误";
    die();
}
$taskStatus = $_GET['status'] ?? null;
if ($taskStatus != 3) {
    echo "任务状态错误";
    die();
}

$dsn = "sqlsrv:Server=localhost;Database=Task";
$db = new PDO($dsn, "sa", "123456");

$sql = "update Task set TaskStatus='{$taskStatus}' where TaskId=" . $taskId;
$result = $db->query($sql);
$classInfo = $result->fetch(PDO::FETCH_ASSOC);
$result = $db->exec($sql);

if ($result) {
    echo "修改任务为完成成功<br />";
    echo "<a href='task_detail.php?id=" . $taskId . "'>返回详情页面</a>";
} else {
    echo "修改任务为完成失败，错误信息为：<pre>{$db->errorInfo()[2]}</pre>";
    echo "<a href='javascript:history.go(-1);'>返回上一页</a>";
}
