<?php
$adminAccount = $_POST['AdminAccount'] ?? '';
if (empty($adminAccount)) {
    echo '参数错误';
    exit();
}
$adminPassword = $_POST['AdminPassword'] ?? '';

$dsn = "sqlsrv:Server=localhost;Database=Task";
$db = new PDO($dsn, "sa", "123456");

// 判断任务是否存在
$selectSql = "select * from Admin where AdminAccount='{$adminAccount}' ";
$statement = $db->query($selectSql);
$adminInfo = $statement->fetch(PDO::FETCH_ASSOC);
if ($adminInfo && $adminInfo['AdminPassword'] == $adminPassword) {
    setcookie("AdminId", $adminInfo['AdminId']);
    setcookie("AdminAccount", $adminInfo['AdminAccount']);
    echo "登录成功<br />";
    echo "<a href='task_list.php'>返回任务列表页面</a>";
    exit();
}
echo "登录失败，账号或者密码错误。";
