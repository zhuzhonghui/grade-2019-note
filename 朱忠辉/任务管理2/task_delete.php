<?php
$taskId = $_GET['id'] ?? null;
if (empty($taskId)) {
    echo "班级id错误";
    die();
}

$dsn = "sqlsrv:Server=localhost;Database=Task";
$db = new PDO($dsn, "sa", "123456");

$sql = "delete FROM Task where TaskId=" . $taskId;
$result = $db->exec($sql);
if ($result) {
    echo "删除成功<br />";
    echo "<a href='task_list.php'>返回列表页面</a>";
} else {
    echo "删除失败，错误信息为：<pre>{$db->errorInfo()[2]}</pre>";
    echo "<a href='javascript:history.go(-1);'>返回上一页</a>";
}
