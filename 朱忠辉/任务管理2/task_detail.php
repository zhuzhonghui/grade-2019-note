<?php
$taskId = $_GET['id'] ?? null;
if (empty($taskId)) {
    echo "任务id错误";
    die();
}

$dsn = "sqlsrv:Server=localhost;Database=Task";
$db = new PDO($dsn, "sa", "123456");

$sql = "SELECT * FROM Task where TaskId=".$taskId;
$result = $db->query($sql);
$taskInfo = $result->fetch(PDO::FETCH_ASSOC);

$statusList = [
    1 => "新创建",
    2 => "进行中...",
    3 => "已完成",
];
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>任务详情</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
    <script src="js/jquery.js"></script>
</head>
<body>
<div id="container">
    <a href="task_list.php">返回任务列表</a>
    <form action="task_add_save.php" method="post">
        <table class="update">
            <caption>
                <h3>任务详情</h3>
            </caption>
            <tr>
                <td>任务名称：</td>
                <td><?php echo $taskInfo['TaskName']; ?></td>
            </tr>
            <tr>
                <td>任务状态：</td>
                <td><?php echo $statusList[$taskInfo['TaskStatus']]; ?></td>
            </tr>
            <tr>
                <td>任务内容：</td>
                <td><textarea name="TaskContent" cols="60" rows="15" readonly="readonly"><?php echo $taskInfo['TaskContent']; ?></textarea></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <?php if ($taskInfo['TaskStatus'] == 1): ?>
                        <a class="update_ing" href="task_update_ing.php?id=<?php echo $taskInfo['TaskId']; ?>&status=2">标记为进行中</a>
                    <?php endif; ?>
                    <?php if ($taskInfo['TaskStatus'] == 2): ?>
                        <a class="update_finish" href="task_update_finish.php?id=<?php echo $taskInfo['TaskId']; ?>&status=3">标记为已完成</a>
                    <?php endif; ?>
                </td>
            </tr>
        </table>
    </form>
</div>
<script src="js/main.js"></script>
</body>
</html>
