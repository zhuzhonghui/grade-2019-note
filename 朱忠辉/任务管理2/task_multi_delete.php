<?php
$taskIds = $_GET['TaskId'] ?? null;
if (empty($taskIds)) {
    echo "任务id错误";
    die();
}

$dsn = "sqlsrv:Server=localhost;Database=Task";
$db = new PDO($dsn, "sa", "123456");

$sql = "delete FROM Task where TaskId in (" . implode(",", $taskIds)  . ") AND TaskStatus=1";
$result = $db->exec($sql);
if ($result) {
    echo "删除成功<br />";
    echo "<a href='task_list.php'>返回列表页面</a>";
} else {
    echo "删除失败，错误信息为：<pre>{$db->errorInfo()[2]}</pre>";
    echo "<a href='javascript:history.go(-1);'>返回上一页</a>";
}
