<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>增加任务</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
</head>
<body>
<div id="container">
    <a href="task_list.php">返回任务列表</a>
    <form action="task_add_save.php" method="post">
        <table class="update">
            <caption>
                <h3>增加任务</h3>
            </caption>
            <tr>
                <td>任务名称：</td>
                <td><input type="text" name="TaskName" /></td>
            </tr>
            <tr>
                <td>任务内容：</td>
                <td><textarea name="TaskContent" cols="60" rows="15"></textarea></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" value="提交" class="btn" />
                    <input type="reset" value="重置" class="btn" />
                </td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>
