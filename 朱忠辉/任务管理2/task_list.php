<?php

if(empty($_COOKIE['AdminId'])) {
    echo "尚未登录，请先登录<br />";
    echo "<a href='login.php'>登录</a>";
    exit();
}

$searchWord = $_GET['search_word'] ?? ''; // 搜索词
$page =$_GET['page'] ?? 1; // 页码

$dsn = "sqlsrv:Server=localhost;Database=Task";
$db = new PDO($dsn, "sa", "123456");

$sql = "SELECT * FROM Task order by TaskId desc offset 0 rows fetch next 50 rows only";
$statement = $db->query($sql);
$taskList = $statement->fetchAll(PDO::FETCH_ASSOC);

$sql2 = "select count(*) as total from Task";
$statement = $db->query($sql2);
$totalResult = $statement->fetch(PDO::FETCH_ASSOC);
$total = $totalResult['total'];

$totalPage = ceil($total / 50);

$statusList = [
    1 => "新创建",
    2 => "进行中...",
    3 => "已完成",
];

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>列表</title>
    <link rel="stylesheet" type="text/css" href="css/main.css"/>
    <script src="js/jquery.js"></script>
</head>
<body>
<div id="container">
    <div id="login_info">
        欢迎你：<?php echo $_COOKIE['AdminAccount']; ?>
        <a href="logout.php">退出登录</a>
    </div>


    <div id="choose_div">
        <button id="all">全选</button>
        <a id="multi_delete" href="javascript:void(0);">删除选中任务</a>

        <form action="task_list.php" method="get" style="display: inline-block; margin: 0 0 0 30px;">
            搜索：<input type="text" name="search_word" value="<?php echo $searchWord ?? ''; ?>" />&nbsp;<input type="submit" value="搜索" />
        </form>

        <a id="add" href="task_add.php" style="float: right">增加任务</a>
    </div>

    <form id="multi-delete-form" action="task_multi_delete.php">
    <table class="list">
        <tr>
            <th></th>
            <th>任务id</th>
            <th>任务名称</th>
            <th>任务状态</th>
            <th>创建时间</th>
            <th>修改时间</th>
            <th>操作</th>
        </tr>
        <?php foreach ($taskList as $item): ?>
            <tr>
                <td><input type="checkbox" class="task-checkbox" name="TaskId[]" value="<?php echo $item['TaskId']; ?>"></td>
                <td><?php echo $item['TaskId']; ?></td>
                <td><?php echo $item['TaskName']; ?></td>
                <td><?php echo $statusList[$item['TaskStatus']]; ?></td>
                <td><?php echo $item['TaskCreateTime']; ?></td>
                <td><?php echo $item['TaskUpdateTime']; ?></td>
                <td>
                    <a class="detail" href="task_detail.php?id=<?php echo $item['TaskId']; ?>">详情</a>
                    <?php if ($item['TaskStatus'] == 1): ?>
                    <a class="delete" href="task_delete.php?id=<?php echo $item['TaskId']; ?>">删除</a>
                    <?php endif;?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <ul id="page">
        <li class="total">总共<?php echo $total; ?>条，共<?php echo $totalPage; ?>页</li>
        <li class="prev"><a href="task_list.php?page=<?php echo ($page - 1) <= 1 ? 1 : ($page - 1); ?>">上一页</a></li>
        <li class="next"><a href="task_list.php?page=<?php echo ($page + 1) >= $totalPage ? $totalPage : ($page + 1); ?>">下一页</a></li>
    </ul>
    </form>
</div>
<script src="js/main.js"></script>
</body>
</html>


