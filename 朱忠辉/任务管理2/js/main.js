$(".update_ing").click(function () {
    if (window.confirm("标记为进行中？")) {
        return true;
    }
    return false;
});

$(".update_finish").click(function () {
    if (window.confirm("标记为已完成？")) {
        return true;
    }
    return false;
});

$(".delete").click(function () {
    if (window.confirm("确认删除吗？")) {
        return true;
    }
    return false;
});

$("#all").click(function () {
    $(".task-checkbox").prop("checked", true);

});

$("#multi_delete").click(function () {
    if (window.confirm("确认批量删除吗？")) {
        $("#multi-delete-form").submit();
    }
});